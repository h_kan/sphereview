#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import healpy as hp

import sys
from ConfigParser import SafeConfigParser

### 使用するパラメータ類をまとめたクラス
class Params:
	def __init__(self, filename):
		# config file の読み込み
		parser = SafeConfigParser()
		parser.read(filename)
		
		input_map = "./input/" + parser.get('igor','input_map')
		input_field = parser.getint('igor','input_field')
		# input map の読み込み
		map_ = hp.read_map(input_map, input_field, nest = False, verbose=False)

		self.output_prefix = "./output/" + parser.get('igor','output_prefix')
		self.div_num = parser.get('igor','div_num')

		min_ = self._arangeParams(parser.get('view','min'), float)
		max_ = self._arangeParams(parser.get('view','max'), float)
		norm = self._arangeParams(parser.get('view','norm'), str)
		doPreview = parser.getboolean('view','preview')

		# fitsMap インスタンスの作成
		self.fmap = fitsMap(map_, min_, max_, norm, doPreview)
	
	# fitsMap インスタンスを返す
	def getMap(self):
		return self.fmap

	# 分割数を返す
	def getDivNum(self):
		Nside = self.fmap.get_nside()
		if self.div_num == 'DEFAULT':
			if Nside < 16:
				div_number = 64
			else:
				div_number = 4 * Nside
		else:
			div_number = int(self.div_num)
		return div_number

	# 読み込んだパラメータを適宜変換するための method
	def _arangeParams(self, par, type_):
		if par == 'None':
			return None
		else:
			return type_(par)
	


### map の見た目を編集するクラス
class fitsMap:
	def __init__(self, map_, min_=None, max_=None, norm=None, doPreview=False):
		self.map_ = map_
		self.min_ = min_
		self.max_ = max_
		self.norm = norm
		self.doPreview = doPreview

	def get_nside(self):
		return hp.get_nside(self.map_)

	# 与えられたパラメータに従いmapの値を変換するmethod
	def convertView(self):
		if self.min_ is not None:
			min_val = float(self.min_)
			print "set min = ", min_val
			self.map_[np.where(self.map_<min_val)] = min_val

		if self.max_ is not None:
			max_val = float(self.max_)
			print "set max = ", max_val
			self.map_[np.where(self.map_>max_val)] = max_val

		if self.norm == 'hist':
			print "hitgram smoothing..."
			max_val = np.max(self.map_)
			min_val = np.min(self.map_)
			npix = hp.nside2npix(hp.get_nside(self.map_))
			sort_ind = np.argsort(self.map_)
			value_arr = np.arange(npix)*(max_val-min_val)/npix + min_val
			self.map_[sort_ind] = value_arr
		elif self.norm is not None:
			print "unkonwn norm is set"

		if self.doPreview:
			self.preview()

	# 現在のmapを表示するmethod
	def preview(self):
		import matplotlib.pyplot as plt
		hp.mollview(self.map_)
		plt.show()
			
		print "これで出力してOK?"
		ans_array = ['y','n']
		input_word = ''

		while input_word not in ans_array:
			print "y->出力 n->中断"
			input_word = raw_input('>>>  ')

		if input_word == ans_array[0]:
			return 0
		elif input_word == ans_array[1]:
			print "rewrite config file!"
			exit()


if __name__ == '__main__':
	argv = sys.argv
	if len(argv) < 2:
		filename = "./config.ini"
	else:
		filename = argv[1]

	### インスタンス化, config・mapの読み込み
	par = Params(filename)
	### mapの取得
	fmap = par.getMap()

	### 読み込んだマップの見た目を変更する
	fmap.convertView()
	Nside = fmap.get_nside()

	### 一周（赤道）を何分割するか
	### 球に見せるためにはある程度以上は必要
	### 高解像度では 4 * Nside を推奨
	div_number = par.getDivNum()
	
	### 極座標で分割設定
	r = 1
	theta = np.arange(div_number+1) * np.array(np.pi / div_number)
	phi = np.arange(div_number+1) * np.array(2*np.pi / div_number)
	
	### 直交座標でのゼロ行列を準備
	x_arr = np.zeros([div_number+1, div_number+1])
	y_arr = np.zeros([div_number+1, div_number+1])
	z_arr = np.zeros([div_number+1, div_number+1])
	
	I_arr = np.zeros([div_number+1, div_number+1])
	
	### りんごの皮むきのごとく
	for i, t in enumerate(theta):
		x = r * np.sin(t) * np.cos(phi)
		y = r * np.sin(t) * np.sin(phi)
		z = r * np.cos(t)
		
		pix_num = hp.vec2pix(Nside, x, y, z)
	
		x_arr[i] = x
		y_arr[i] = y
		z_arr[i] = z
	
		I_arr[i] = fmap.map_[pix_num]
	
	### csvファイルに保存
	prefix = par.output_prefix
	np.savetxt(prefix + "_x.csv", x_arr, delimiter=",")
	np.savetxt(prefix + "_y.csv", y_arr, delimiter=",")
	np.savetxt(prefix + "_z.csv", z_arr, delimiter=",")
	np.savetxt(prefix + "_I.csv", I_arr, delimiter=",")
	
	print "output complete!"
	
